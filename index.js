// B200 > s5 > solution > index.js


class Customer {
	constructor(email) {
		this.email = email
		this.cart = new Cart()
		this.orders = []

	}
	checkOut() {
		if(this.cart.contents.length > 0) {
			this.orders.push({
				products: this.cart.contents,
				totalAmount: this.cart.computeTotal().totalAmount
			})
		}
		this.cart = new Cart()
		return this
	}
} // ==> end of "class Customer"

class Cart {
	constructor() {
		this.contents = []
		this.totalAmount = 0
	}
	

	addToCart(product, quantity) {
		this.contents.push({
			product: product,
			quantity: quantity
		})
		return this
	}


	showCartContents() {
    	console.log(this.contents)
    	return this
	}
	

	updateProductQuantity(name, newQuantity) {
		this.contents.find(item => item.product.name === name).quantity = newQuantity
		return this
	}

	clearCartContents() {
		this.contents = []
		return this
	}

	computeTotal() {
		this.totalAmount = 0 
		
		if(this.contents.length > 0) {
			this.contents.forEach(item => {
				this.totalAmount = this.totalAmount + (item.product.price * item.quantity)
			})
		} 
		return this
	}

} // ==> end of "class Cart"

class Product {
	constructor(name, price) {
		this.name = name 
		this.price = price
		this.isActive = true
	}
	archive() {
		if(this.isActive) {
			this.isActive = false
		}
		return this
	}

	updatePrice(newPrice) {
		this.price = newPrice
		return this
	}
} // ==> end of "class Product"



let bert = new Customer("bert@mail.com")
let alice = new Customer("alice@mail.com")

let prodA = new Product("coffee", 5.30)
let prodB = new Product("sugar", 4.00)
let prodC = new Product("creamer", 6.00)

prodC.archive()

alice.cart.addToCart(prodA, 5)
alice.cart.addToCart(prodB, 4)
bert.cart.addToCart(prodB, 1)


console.log(alice)
console.log(bert)

